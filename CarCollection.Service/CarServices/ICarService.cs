﻿using CarCollection.Service.CarServices.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarCollection.Service.CarServices
{
    public interface ICarService
    {
        Task<int> CreateCar(CarDto car);
        Task UpdateCar(CarDto car);
        Task DeleteCar(int id);
        Task<CarDto> GetCar(int id);
        Task<List<CarDto>> GetAll(CarFilterDto carFilter);
    }
}
