﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarCollection.Service.CarServices.Dto
{
    public class CarDto
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public bool New { get; set; }
        public int UserId { get; set; }
    }


    public class CarFilterDto
    {
        public int UserId { get; set; }
        public string Search { get; set; }
    }
}
