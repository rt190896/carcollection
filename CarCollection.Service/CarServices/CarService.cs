﻿using AutoMapper;
using CarCollection.Data.Entities;
using CarCollection.Data.UnitofWork;
using CarCollection.Service.CarServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCollection.Service.CarServices
{
    public class CarService : ICarService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CarService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<int> CreateCar(CarDto car)
        {
            try
            {
                var data = _mapper.Map<Cars>(car);
                await _unitOfWork.RepositoryEntity.Create(data);
                _unitOfWork.SaveChanges();
                return data.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteCar(int id)
        {
            try
            {
                await _unitOfWork.RepositoryEntity.Delete<Cars>(id);
                _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<CarDto>> GetAll(CarFilterDto carFilter)
        {
            var list = _unitOfWork.RepositoryEntity.GetAll<Cars>().Where(x => x.UserId == carFilter.UserId);
            if (!string.IsNullOrEmpty(carFilter.Search))
            {
                list = list.Where(x => carFilter.Search.Contains(x.Model) || carFilter.Search.Contains(x.Brand));
            }

            var dtolist = _mapper.Map<List<CarDto>>(list);
            return dtolist;
        }

        public async Task<CarDto> GetCar(int id)
        {
            var data = await _unitOfWork.RepositoryEntity.GetById<Cars>(id);
            var dtolist = _mapper.Map<CarDto>(data);
            return dtolist;
        }

        public async Task UpdateCar(CarDto car)
        {
            try
            {
                var data = await _unitOfWork.RepositoryEntity.GetById<Cars>(car.Id);
                data.New = car.New;
                data.Model = car.Model;
                data.Price = car.Price;
                data.UserId = car.UserId;
                data.Brand = car.Brand;
                await _unitOfWork.RepositoryEntity.Update(data);
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
