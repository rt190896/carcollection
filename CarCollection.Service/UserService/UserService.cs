﻿using AutoMapper;
using CarCollection.Data.Entities;
using CarCollection.Data.UnitofWork;
using CarCollection.Service.UserService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCollection.Service.UserService
{
    public class UserService : IUserService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<int> CreateUser(UserDto user)
        {
            try
            {
                var data = _mapper.Map<Users>(user);
                await _unitOfWork.RepositoryEntity.Create(data);
                _unitOfWork.SaveChanges();
                return data.Id;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task DeleteUser(int id)
        {
            try
            {
                await _unitOfWork.RepositoryEntity.Delete<Users>(id);
                _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<UserDto>> GetAll()
        {
            var list = _unitOfWork.RepositoryEntity.GetAll<Users>();
            var dtolist = _mapper.Map<List<UserDto>>(list);
            return dtolist;
        }

        public async Task<UserDto> GetUser(int id)
        {
            var data = await _unitOfWork.RepositoryEntity.GetById<Users>(id);
            var dtolist = _mapper.Map<UserDto>(data);
            return dtolist;
        }

        public async Task UpdateUser(UserDto user)
        {
            try
            {
                var data = await _unitOfWork.RepositoryEntity.GetById<Users>(user.Id);
                data.UserName = user.UserName;
                data.Password = user.Password;
                await _unitOfWork.RepositoryEntity.Update(data);
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<(bool result, int id)> Login(UserDto user)
        {
            try
            {
                var data = _unitOfWork.RepositoryEntity.GetAll<Users>().Where(x => x.UserName == user.UserName && x.Password == user.Password);
                if (data.Count() > 0)
                {
                    return (true, data.FirstOrDefault().Id);
                }
                else
                {
                    return (false, 0);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
