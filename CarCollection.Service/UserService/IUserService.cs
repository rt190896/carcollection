﻿using CarCollection.Service.UserService.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarCollection.Service.UserService
{
    public interface IUserService
    {
        Task<int> CreateUser(UserDto user);
        Task UpdateUser(UserDto user);
        Task DeleteUser(int id);
        Task<UserDto> GetUser(int id);
        Task<List<UserDto>> GetAll();

        Task<(bool result, int id)> Login(UserDto user);
    }
}
