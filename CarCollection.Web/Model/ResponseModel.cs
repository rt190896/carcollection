﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarCollection.Web.Model
{
    public class ResponseModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
