﻿using AutoMapper;
using CarCollection.Data.Entities;
using CarCollection.Service.UserService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarCollection.Web.Automapper
{
    public class UserMap : Profile
    {
        public UserMap()
        {
            CreateMap<UserDto, Users>().ReverseMap();
        }
    }
}
