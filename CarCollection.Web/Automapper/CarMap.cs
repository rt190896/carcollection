﻿using AutoMapper;
using CarCollection.Data.Entities;
using CarCollection.Service.CarServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarCollection.Web.Automapper
{
    public class CarMap : Profile
    {
        public CarMap()
        {
            CreateMap<CarDto, Cars>().ReverseMap();
        }
    }
}
