﻿using CarCollection.Service.CarServices;
using CarCollection.Service.CarServices.Dto;
using CarCollection.Service.UserService.Dto;
using CarCollection.Web.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CarCollection.Web.Controllers
{
    [Route("api/car")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly ICarService _carService;
        public CarsController(ICarService carService)
        {
            _carService = carService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CarDto car)
        {
            await _carService.CreateCar(car);
            return Ok(new ResponseModel() { Status = (int)HttpStatusCode.OK, Message = "Car Created Successfully!" });
        }

        [HttpPut]
        public async Task<IActionResult> Update(CarDto car)
        {
            await _carService.UpdateCar(car);
            return Ok(new ResponseModel() { Status = (int)HttpStatusCode.OK, Message = "Car Updated Successfully!" });
        }


        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var data = await _carService.GetCar(id);
            return Ok(data);
        }

        [HttpPost("getall")]
        public async Task<IActionResult> GetAll(CarFilterDto carFilter)
        {
            var data = await _carService.GetAll(carFilter);
            return Ok(data);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _carService.DeleteCar(id);
            return Ok(new ResponseModel() { Status = (int)HttpStatusCode.OK, Message = "Car Deleted Successfully!" });
        }

    }
}
