﻿using CarCollection.Service.UserService;
using CarCollection.Service.UserService.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarCollection.Web.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserDto car)
        {
            await _userService.CreateUser(car);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update(UserDto car)
        {
            await _userService.UpdateUser(car);
            return Ok();
        }


        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var data = await _userService.GetUser(id);
            return Ok(data);
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            var data = await _userService.GetAll();
            return Ok(data);
        }

        [HttpDelete("delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);
            return Ok();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserDto user)
        {
            var result = await _userService.Login(user);
            if (result.result)
            {
                return Ok(result.id);
            }
            else
            {
                return Ok(0);
            }
        }

    }
}
