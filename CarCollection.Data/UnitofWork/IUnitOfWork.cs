﻿using CarCollection.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarCollection.Data.UnitofWork
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        IRepositoryEntity RepositoryEntity { get; }
    }
}
