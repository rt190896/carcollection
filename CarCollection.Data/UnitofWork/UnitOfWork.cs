﻿using CarCollection.Data.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarCollection.Data.UnitofWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            RepositoryEntity = new Repository.RepositoryEntity(context);
        }

        public IRepositoryEntity RepositoryEntity { get; private set; }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
