﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCollection.Data.Repository
{
    public class RepositoryEntity:IRepositoryEntity
    {
        private readonly DbContext _dbContext;

        public RepositoryEntity(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> GetAll<T>()
            where T : class
        {
            return _dbContext.Set<T>().AsQueryable();
        }

        public async Task<T> GetById<T>(int id)
          where T : class
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task Create<T>(T entity)
            where T : class
        {
            await _dbContext.Set<T>().AddAsync(entity);
        }

        public Task Update<T>(T entity)
            where T : class
        {
            _dbContext.Set<T>().Update(entity);

            return Task.CompletedTask;
        }

        public async Task Delete<T>(int id)
           where T : class
        {
            var entity = await GetById<T>(id);
            _dbContext.Set<T>().Remove(entity);
        }
    }
}
